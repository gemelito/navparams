import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';

/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {
  public HomePage = HomePage;
  public date: string;
  public name: string;
  public description: string;
  public allParams;

  public model = { date: "", name: "", description: "" };

  public List: Array<{ date: string, name: string, description: string }> = new Array();

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.date = this.navParams.get("date");
    this.name = this.navParams.get("name");
    this.description = this.navParams.get("name");
    this.allParams = this.navParams.data;
    this.model;
  }

  ionViewDidLoad() {
  }

  save(){
    this.List.push(this.model);
    // this.navCtrl.pop();
    this.navCtrl.setRoot(HomePage, this.List, { animate: true, duration: 1000 });

  }

  goBack() {
    this.navCtrl.pop();
  }

}
