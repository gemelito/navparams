import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public aboutPage = AboutPage;
  public contactPage = ContactPage;

  public Params = {
    date: "2019-05-21",
    name: "Misael",
    description: "Esta es la descripción",
  };

  public model: any;

  public actions: any;

  public List: Array<{date: string, name: string, description: string }> = new Array();

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.model = this.Params;

    this.List.push(this.model);

    // if (  !this.isEmpty(this.navParams.data) ){
    if ( this.navParams.data.length ) {
      this.navParams.data.forEach(element => {
        this.List.push(element);
      });
    }

    this.actions = this.List;

  }

  // Paso de parametros
  public gotoAbout() {
    this.navCtrl.push( this.aboutPage, this.Params);
  }

  public gotoContact() {
    this.navCtrl.push(ContactPage);
  }

  public isEmpty(obj) {
  for (var key in obj) {
    if (obj.hasOwnProperty(key)){
      // console.log("Any 2");
      return false;
    }

  }
  // console.log("Any");
  return true;
}

}
